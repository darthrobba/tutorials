# Create Stack
aws cloudformation create-stack  --stack-name robsLaunchTemplate   --capabilities "CAPABILITY_NAMED_IAM" "CAPABILITY_AUTO_EXPAND"  --template-body file://LaunchTemplate.json  --parameters  ParameterKey=KeyName,ParameterValue=CloudFormationKP   ParameterKey=InstanceType,ParameterValue=t1.micro

# Update Stack
aws cloudformation update-stack  --stack-name robsLaunchTemplate   --capabilities "CAPABILITY_NAMED_IAM" "CAPABILITY_AUTO_EXPAND"  --template-body file://LaunchTemplate.json  --parameters  ParameterKey=KeyName,ParameterValue=CloudFormationKP   ParameterKey=InstanceType,ParameterValue=t1.micro


# Stack Info
aws cloudformation describe-stacks --stack-name robsLaunchTemplate


aws cloudformation list-stacks --query 'StackSummaries[?StackName==`UpdateTutorial-010320201`].{Name:StackName,Status:StackStatus}'  --output text

aws cloudformation  describe-stacks --stack-name ubuntuSystemsManager --query 'Stacks[0].{Name:StackName,Status:StackStatus}' --output text

aws cloudformation  describe-stacks --stack-name ubuntuSystemsManager

aws cloudformation  describe-stacks --stack-name UpdateTutorial-010320201 --query 'Stacks[0].Outputs[?OutputKey==`WebsiteURL`].{URL:OutputValue}'  --output text

while [ 1 -eq 1  ] ; do aws cloudformation  describe-stacks --stack-name robsLaunchTemplate --query 'Stacks[0].{Name:StackName,Status:StackStatus}' --output text ; sleep 3 ; done




###  Register Patch Group ###
aws ssm register-patch-baseline-for-patch-group --baseline-id "arn:aws:ssm:us-west-1:433649990780:patchbaseline/pb-0ceaff147dad553bb" --patch-group "Production"
